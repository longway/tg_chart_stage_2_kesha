Telegram Stage 2 Contest (April 2019).

Library for charts: tgchart.js

Author: https://t.me/skill_surf

See live example: http://outstanding-earth.surge.sh/



Project is a library to showing charts.

Features:

* 5 chart types
* Small size: 31Kb after minification, 10Kb gzip
* Fast and performant: 3 canvas - main chart, preview chart, "zooming-area"
* Cross-browser support: >98% of browsers supported


Usage:

1) Import chart from library

import TgChart from 'tgchart'

2) Create chart at html node:
var chartsNode = document.getElementById(...)
var chart = new TgChart(chartsNode, 'MyChartName')

3) Set data

chart.setData(data)

4) Change theme (if you like):

chart.setTheme(TgChart.THEMES.DARK) - dark theme
chart.setTheme(TgChart.THEMES.LIGHT) - light theme

5) Change tooltip and checkboxes
Use CSS to change the style of the tooltip and checkboxes.

6) Destroy (if you need):

chart.destroy() - removes all created elements and event listeners


DEVELOPMENT

1) yarn install
2) "yarn run dev" - bundle & runs dev server
3) "yarn run build" - bundle not minified version
4) "yarn run deploy" - bundle, minify & deploys server
