import TgChart from './tgchart/tgchart'


var lightTheme = true
var charts = []

window.onChangeTheme = function(e) {
    e = e || window.event
    var target = e.target || e.srcElement

    lightTheme = !lightTheme
    if (lightTheme) {
        target.innerText = 'Switch to Night Mode'
        document.body.classList.remove('dark-theme')
    } else {
        target.innerText = 'Switch to Day Mode'
        document.body.classList.add('dark-theme')
    }
    for (var i in charts) {
        var chart = charts[i]
        chart.setTheme(lightTheme ? TgChart.THEMES.LIGHT : TgChart.THEMES.DARK)
    }
}

var chartsToFectch = [
  ['json_data/1/overview.json', 'Followers'], // NOTE: LOAD 1st LINEAR CHART WITH 1 Y
  ['json_data/2/overview.json', 'Interactions'], // NOTE: LOAD 2st LINEAR CHART WITH 2 Y
  ['json_data/3/overview.json', 'Fruits left'], // NOTE: LOAD 3rd STACKED CHART WITH 1 Y
  ['json_data/4/overview.json', 'Views'], // NOTE: LOAD 4th BARS CHART WITH 1 Y
  ['json_data/5/overview.json', 'Fruits bought'], // NOTE: LOAD 5th PERCENTAGE CHART WITH 1 Y
]

function tryRemoveLoader() {
  if( chartsToFectch.length > 0 ) { return }

  var preloaderNode = document.getElementById('preloader')
  preloaderNode.parentNode.removeChild(preloaderNode)
  document.getElementById('buttons').style.display = 'block'
}

function fetchNextJsonAndDrawChart() {
  if( chartsToFectch.length === 0 ) { return }

  var urlAndName = chartsToFectch.shift()

  var request = new XMLHttpRequest()
  request.onreadystatechange = function () {
      if (request.readyState === 4 && request.status === 200) {
          var data = JSON.parse(request.responseText)
          var chartsNode = document.getElementById('charts')

          var chart = new TgChart(chartsNode, urlAndName[1])
          chart.setData(data)
          charts.push(chart)

          tryRemoveLoader()
          fetchNextJsonAndDrawChart()
      }
  }

  request.open('GET', urlAndName[0], true)
  request.send()
}


fetchNextJsonAndDrawChart()
