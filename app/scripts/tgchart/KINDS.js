export const KINDS = {
  LINEAR: 'linear',
  LINEAR_2Y: 'linear2y',
  BARS: 'bars',
  STACKED: 'stacked',
  PERCENTAGE: 'percentage',
}
