export default function createElement(parent, tag, clazz, prepend = false) {
    const element = document.createElement(tag)
    if (clazz && typeof clazz === 'string') {
      clazz = clazz.trim()
      clazz.split(' ').forEach(_clazz => element.classList.add(_clazz))
    }

    if( prepend ) {
      return parent.insertBefore(element, parent.firstChild)
    }

    return parent.appendChild(element)
}
