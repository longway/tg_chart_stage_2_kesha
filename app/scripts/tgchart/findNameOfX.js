export default function findNameOfX(types) {
    for (var name in types) {
        if (types[name] === 'x') return name
    }
    return null
}
