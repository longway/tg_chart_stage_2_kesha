const MONTH_NAMES_SHORT = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
const MONTH_NAMES_LONG = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
const DAY_NAMES = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']

export default function formatDate(time, short, fullMonth = false) {
    const date = new Date(time)
    const _monthNum = date.getMonth()
    let s = `${ date.getDate() } ${ fullMonth ? MONTH_NAMES_LONG[_monthNum] : MONTH_NAMES_SHORT[_monthNum] }`

    if (short) { return s }

    if( !fullMonth ) {
      s = DAY_NAMES[date.getDay()] + ', ' + s
    }

    s += ' ' + date.getFullYear()

    return s
}
