export default function mainToScreenY(y, mainScaleY, mainOffsetY) {
    return y * mainScaleY + mainOffsetY
}
