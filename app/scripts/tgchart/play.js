export default function play(time, anim, toValue) {
    anim.startTime = time
    anim.fromValue = anim.value
    anim.toValue = toValue
}
