export default function renderBars(
  context,
  chartWidth,
  xColumn,
  yColumn,
  minI,
  maxI,
  scaleX,
  scaleY,
  offsetX,
  offsetY,
  paddingHor,
  alpha,
  chartKind,
  chartMaxY,
  selectI = -1,
  horOffset = 0,
) {
    let step = Math.floor((maxI - minI) / chartWidth)
    if (step < 1) step = 1

    let rectsWidth = 0

    context.fillStyle = yColumn.color
    if( selectI === -1 ) {
      context.globalAlpha = alpha
    }

    for (var i = minI; i < maxI; i += step) {
        const x = xColumn.data[i]
        const y = yColumn.data[i]

        const rectStartX = Math.floor(x * scaleX + offsetX)
        const rectStartY = Math.floor(y * scaleY + offsetY)


        let rectWidth
        const nextX = xColumn.data[i + step]
        if( i + step < maxI ) {
          rectWidth = Math.floor((nextX * scaleX + offsetX) - rectStartX)
          rectsWidth += rectWidth
          if( rectStartX < 0 ) { rectsWidth += rectStartX }
        } else {
          rectWidth = chartWidth - rectsWidth + horOffset
        }

        if( selectI > -1 ) {
          const isHovering = selectI === i
          context.globalAlpha = alpha * (isHovering ? 1 : 0.7)
        }

        const rectHeight = chartMaxY - rectStartY
        context.fillRect(rectStartX, rectStartY, rectWidth, rectHeight)
    }
}
