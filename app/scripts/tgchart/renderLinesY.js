import mainToScreenY from './mainToScreenY'


export default function renderLinesY(
  context,
  textY,
  textCountY,
  mainMinY,
  mainScaleY,
  mainOffsetY,
  paddingHor,
  width,
  withLastLine = false
) {
    if (textY.alpha.value === 0) { return }

    context.globalAlpha = textY.alpha.value

    for (var i = 1; i < textCountY + ( withLastLine ? 1 : 0 ); i++) {
        var value = mainMinY.value + textY.delta * i
        const y = mainToScreenY(value, mainScaleY, mainOffsetY)

        context.beginPath()
        context.moveTo(paddingHor, y)
        context.lineTo(width - paddingHor, y)
        context.stroke()
    }
}
