export default function renderPath(
  context,
  chartWidth,
  xColumn,
  yColumn,
  minI,
  maxI,
  scaleX,
  scaleY,
  offsetX,
  offsetY,
  alpha,
) {
    let step = Math.floor((maxI - minI) / chartWidth)
    if (step < 1) step = 1

    let startX = xColumn.data[minI]
    let startY = yColumn.data[minI]

    context.strokeStyle = yColumn.color
    context.lineJoin = 'bevel'
    context.lineCap = 'butt'
    context.globalAlpha = alpha

    context.beginPath()
    context.moveTo(startX * scaleX + offsetX, startY * scaleY + offsetY)
    for (var i = minI + step; i < maxI; i += step) {
        var x = xColumn.data[i]
        var y = yColumn.data[i]
        context.lineTo(x * scaleX + offsetX, y * scaleY + offsetY)
    }
    context.stroke()
}
