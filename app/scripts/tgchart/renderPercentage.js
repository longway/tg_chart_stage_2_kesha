export default function renderPercentage(
  context,
  width,
  columns,
  minI,
  maxI,
  paddingHor,
  xColumn,

  scaleX,
  offsetX,

  scaleY,
  offsetY,

  chartHeight,
  chartOffsetY,
  isPreview,
  selectX = null
) {
  let step = Math.floor((maxI - minI) / (width - paddingHor * 2))
  if (step < 1) step = 1

  let maxYValues = {}
  let savedPaths = {}

  const chartMaxY = chartOffsetY + chartHeight

  const alphaColumn = isPreview ? 'previewAlpha' : 'alpha'

  for (let _i = minI; _i < maxI; _i += step) {
    let _maxY = 0
    for (var _colI = 0; _colI < columns.length; _colI++) {
      const yColumn = columns[_colI]
      _maxY += yColumn.data[_i] * yColumn[alphaColumn].value
    }

    maxYValues[_i] = _maxY
    savedPaths[_i] = 0
  }

  context.globalAlpha = 1
  context.lineJoin = 'bevel'
  context.lineCap = 'butt'
  context.lineWidth = 0
  context.strokeStyle = 'transparent'

  for (var _colI = 0; _colI < columns.length; _colI++) {
      const yColumn = columns[_colI]

      let startX = xColumn.data[minI] * scaleX + offsetX
      let startY = (yColumn.data[minI] / maxYValues[minI]) * chartHeight * yColumn[alphaColumn].value

      context.fillStyle = yColumn.color

      context.beginPath()

      context.moveTo(startX, chartMaxY - savedPaths[minI])
      savedPaths[minI] += startY

      if( _colI === 0 ) { // NOTE: GO RIGHT TO END
        context.lineTo(xColumn.data[maxI - step] * scaleX + offsetX, chartMaxY)
      } else { // NOTE: REPEAT PATH OF PREV AREA
        for (let _i = minI + step; _i < maxI; _i += step) {
          let nextX = xColumn.data[_i] * scaleX + offsetX
          let savedY = savedPaths[_i]

          context.lineTo(nextX, chartMaxY - savedY)
        }
      }

      if( _colI === columns.length - 1 ) {
        context.lineTo(xColumn.data[maxI - step] * scaleX + offsetX, chartOffsetY)
      } else {
        // NOTE: CREATE UPPER PATH IN REVERSE ORDER
        for (let _i = minI + 1; _i < maxI; _i += step) {
            let _iReversed = maxI + minI - _i

            let nextX = xColumn.data[_iReversed] * scaleX + offsetX
            let nextY = (chartHeight * yColumn.data[_iReversed] / maxYValues[_iReversed] ) * yColumn[alphaColumn].value

            // TODO
            // if( _colI === columns.length - 1 ) {
            //   nextY = Math.ceil(nextY)
            // } else {
            //   nextY = Math.floor(nextY)
            // }

            savedPaths[_iReversed] += nextY
            context.lineTo(nextX, chartMaxY - savedPaths[_iReversed])
        }
      }

      context.lineTo(startX, chartMaxY - savedPaths[minI])

      context.fill()
  }
}
