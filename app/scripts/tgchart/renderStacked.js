export default function renderStacked(
  context,
  chartWidth,
  columns,
  minI,
  maxI,
  xColumn,

  scaleX,
  offsetX,

  scaleY,
  offsetY,

  chartHeight,
  chartOffsetY,
  pixelRatio,
  isPreview,
  selectI = -1,
  horOffset = 0,
) {
  let rectsWidth = 0
  const chartMaxY = chartHeight + chartOffsetY

  let step = Math.floor((maxI - minI) / chartWidth)
  if (step < 1) step = 1

  let columnOffsetY = 0

  let nextX = null

  // NOTE: WITH "previewAlpha" ANIMATION OF PREVIEW HEIGHT GOES NOT SIMILAR TO MAIN CHART
  const alphaValueName = isPreview ? 'alpha' : 'alpha'


  for (let i = minI; i < maxI; i += step) {
    let startX = xColumn.data[i]
    nextX = xColumn.data[i + step]

    for (var _colI = 0; _colI < columns.length; _colI++) {
      const yColumn = columns[_colI]

      const startY = yColumn.data[i] * yColumn[alphaValueName].value
      const rectStartX = Math.floor(startX * scaleX + offsetX)
      let rectStartY = Math.floor(startY * scaleY + offsetY)

      let rectWidth

      if( i + step < maxI ) {
        rectWidth = Math.floor((nextX * scaleX + offsetX) - rectStartX)
        if( _colI === 0 ) {
          rectsWidth += rectWidth
          if( rectStartX < 0 ) { rectsWidth += rectStartX }
        }
      } else {
        rectWidth = chartWidth - rectsWidth + horOffset
      }

      const rectHeight = chartHeight - rectStartY
      if( rectHeight < pixelRatio ) { rectHeight = pixelRatio * yColumn[alphaValueName].value }

      if( selectI === -1 ) {
        context.globalAlpha = 1
      } else {
        const isHovering = selectI === i
        context.globalAlpha = yColumn[alphaValueName].value * (isHovering ? 1 : 0.7)
      }
      context.fillStyle = yColumn.color

      let _rectStartY = rectStartY + columnOffsetY * scaleY
      _rectStartY += chartOffsetY
      if( _rectStartY < 0 ) { _rectStartY = 0 }

      context.fillRect(rectStartX, _rectStartY, rectWidth, rectHeight)

      columnOffsetY += startY
    }

    columnOffsetY = 0
  }
}
