import mainToScreenY from './mainToScreenY'
import formatNumber from './formatNumber'


export default function renderTextsY(
  context,
  textY,
  textCountY,
  mainScaleY,
  mainOffsetY,
  paddingHor,
  textYMargin,
  mainMinY,
  inverse = false,
  withLastLine = false
) {
    if (textY.alpha.value === 0) { return }

    context.globalAlpha = inverse ? (1 - textY.alpha.value ) : textY.alpha.value

    for (let i = 1; i < textCountY + (withLastLine ? 1 : 0); i++) {
        const value = mainMinY.value + textY.delta * i
        const y = mainToScreenY(value, mainScaleY, mainOffsetY)
        context.fillText(formatNumber(value, true), paddingHor, y + textYMargin)
    }
}
