import formatDate from './formatDate'
import formatNumber from './formatNumber'
import createElement from './createElement'
import removeAllChild from './removeAllChild'
import addEventListener from './addEventListener'
import removeEventListener from './removeEventListener'
import createAnimation from './createAnimation'
import play from './play'
import updateAnimation from './updateAnimation'
import renderPath from './renderPath'
import renderBars from './renderBars'
import renderStacked from './renderStacked'
import renderPercentage from './renderPercentage'
import findNameOfX from './findNameOfX'
import roundRect from './roundRect'
import renderLinesY from './renderLinesY'
import renderTextsY from './renderTextsY'
import { KINDS } from './KINDS'


const THEMES = {
  LIGHT: 'light',
  DARK: 'dark',
}
const WHITE_COLOR = '#ffffff'
const THEMES_COLORS = {}
THEMES_COLORS[THEMES.LIGHT] = {
  circleFill: WHITE_COLOR,
  line: 'rgba(24, 45, 59, 0.1)',
  zeroLine: '#ecf0f3',
  selectLine: '#dfe6eb',
  text: '#8E8E93',

  preview: '#E2EEF9',
  previewAlpha: 0.6,
  previewBorderAlpha: 1.0,
  previewBorder: '#C0D1E1',
  previewUiWTouchLine: WHITE_COLOR,
  previewUiWTouchAlpha: 1.0,

  background: WHITE_COLOR,
}
THEMES_COLORS[THEMES.DARK] = {
  circleFill: '#242f3e',
  line: 'rgba(255, 255, 255, 0.1)',
  zeroLine: '#313d4d',
  selectLine: '#3b4a5a',
  text: 'rgba(163, 177, 194, 0.6)',


  preview: '#304259',
  previewAlpha: 0.6,
  previewBorderAlpha: 1.0,
  previewBorder: '#56626D',
  previewUiWTouchLine: WHITE_COLOR,
  previewUiWTouchAlpha: 1.0,

  background: '#242f3e',
}


export default function TgChart(parentContainer, name = '') {
    const container = createElement(parentContainer, 'div', 'tgchart')

    const chartHead = createElement(container, 'div', 'head')

    const chartTitle = createElement(chartHead, 'div', 'chart-title')
    chartTitle.innerText = name.length > 0 ? name : 'TgChart'

    const datesRangesTitle = createElement(chartHead, 'div', 'dates-range')

    this.theme = THEMES.LIGHT
    this.isThemeLight = true
    this.isThemeDark = false

    this.setChartKind = (chartKind) => {
      this.chartKind = chartKind
      this.isChartKind.linear1Y = chartKind === KINDS.LINEAR
      this.isChartKind.linear2Y = chartKind === KINDS.LINEAR_2Y
      this.isChartKind.bars = chartKind === KINDS.BARS
      this.isChartKind.stacked = chartKind === KINDS.STACKED
      this.isChartKind.percentage = chartKind === KINDS.PERCENTAGE
    }
    this.isChartKind = {}
    this.setChartKind(KINDS.LINEAR)

    const mainChartCanvas = createElement(container, 'canvas', 'main-chart')
    const previewContainer = createElement(container, 'div', 'preview')
    const canvases = {
      main: mainChartCanvas,
      previewChart: createElement(previewContainer, 'canvas', 'preview-chart'),
      previewMover: createElement(previewContainer, 'canvas', 'preview-mover'),
    }
    const contexts = {
      main: canvases.main.getContext('2d'),
      previewChart: canvases.previewChart.getContext('2d'),
      previewMover: canvases.previewMover.getContext('2d'),
    }

    const checksContainer = createElement(container, 'div', 'checks')

    const popup = createElement(container, 'div', 'popup')
    popup.style.display = 'none'
    let popupTitleDate = null

    var colors = null
    var data = null
    var xColumn = null
    var columns = null
    let popupLabels = null
    let popupValues = null
    let popupPercentages = null
    let checkboxesShakeTimeouts = null

    var width = 0
    let mainChartWidth = 0
    let previewChartWidth = 0
    var height = 0
    var mainHeight = 0

    var textCountX = 6
    var textCountY = 6

    const SCALE_DURATION = 400
    const TEXT_X_FADE_DURATION = 200

    const pixelRatio = window.devicePixelRatio


    const previewUiW = 10 * pixelRatio
    const previewUiWTouchLineW = 2 * pixelRatio
    const previewUiWTouchLineH = 12 * pixelRatio
    const previewUiWTouchLineRadius = 2
    const previewUiRadius = 8 * pixelRatio
    const previewBorderRadius = previewUiRadius - 1 * pixelRatio

    const previewUiH = 2 * pixelRatio
    const previewChartH = 38 * pixelRatio
    const previewHeight = previewChartH + previewUiH * 2

    var mouseArea = 20 * pixelRatio
    var lineWidth = 1 * pixelRatio
    var previewLineWidth = 1 * pixelRatio
    var mainLineWidth = 2 * pixelRatio
    var circleRadius = 3 * pixelRatio
    var circleLineWidth = 4 * pixelRatio
    const font = (10 * pixelRatio) + 'px Arial'
    const textYMargin = -6 * pixelRatio
    const textXMargin = 16 * pixelRatio
    const textXWidth = 30 * pixelRatio
    const textYHeight = 45 * pixelRatio
    const mainPaddingTop = 21 * pixelRatio
    const mainPaddingBottom = 10 * pixelRatio
    const paddingHor = 11 * pixelRatio
    const popupLeftMargin = 15
    const popupTopMargin = 25
    const popupTextYWidth = paddingHor + 10 * pixelRatio

    var intervalX = 0
    let forceMinY = true

    var mainMinX = 0
    var mainMaxX = 0
    var mainRangeX = 0

    const yAxisNames = ['y0', 'y1']

    const mainMinYDefault = createAnimation(0, SCALE_DURATION)
    mainMinYDefault.value = mainMinYDefault.toValue

    const mainMinY = {
      y0: createAnimation(0, SCALE_DURATION),
      y1: createAnimation(0, SCALE_DURATION),
    }
    var mainMaxY = {
      y0: 0,
      y1: 0,
    }
    const mainRangeY = {
      y0: createAnimation(0, SCALE_DURATION),
      y1: createAnimation(0, SCALE_DURATION),
    }

    var mainScaleX = 1
    let mainScaleByAvgRectWidth = 1
    var mainOffsetX = 0

    var mainMinI = 0
    var mainMaxI = 0


    const previewMinY = {
      y0: createAnimation(0, SCALE_DURATION),
      y1: createAnimation(0, SCALE_DURATION),
    }
    var previewMaxY = {
      y0: 0,
      y1: 0,
    }
    const previewRangeY = {
      y0: createAnimation(0, SCALE_DURATION),
      y1: createAnimation(0, SCALE_DURATION),
    }

    var previewMinX = 0
    var previewMaxX = 0
    var previewRangeX = 0

    var previewScaleX = 1
    let previewScaleByAvgRectWidth = 1
    var previewScaleY = {
      y0: 1,
      y1: 1,
    }

    var previewOffsetX = 0
    var previewOffsetY = {
      y0: 0,
      y1: 0,
    }


    var selectX = 0
    var selectY = 0
    var selectI = 0

    var oldTextX = {delta: 1, alpha: createAnimation(0, TEXT_X_FADE_DURATION)}
    var newTextX = {delta: 1, alpha: createAnimation(0, TEXT_X_FADE_DURATION)}

    const oldTextY = {
      y0: { delta: 1, alpha: createAnimation(0, SCALE_DURATION) },
      y1: { delta: 1, alpha: createAnimation(0, SCALE_DURATION) },
    }
    const newTextY = {
      y0: { delta: 1, alpha: createAnimation(0, SCALE_DURATION) },
      y1: { delta: 1, alpha: createAnimation(0, SCALE_DURATION) },
    }

    var needRedrawMain = true
    var needRedrawPreviewChart = true
    var needRedrawPreviewMover = true

    const canvasBounds = {
      main: { width: 0, height: 0, left: 0, top: 0 },
      previewChart: { width: 0, height: 0, left: 0, top: 0 },
      previewMover: { width: 0, height: 0, left: 0, top: 0 },
    }

    var newMouseX = 0
    const newMouseY = {
      main: 0,
      previewChart: 0,
      previewMover: 0,
    }
    var mouseX = newMouseX
    let mouseY = newMouseY
    var mouseStartX = 0
    var mouseRange = 0
    var previewUiMin = 0
    var previewUiMax = 0

    var time = 0

    var NONE = 0
    var DRAG_START = 1
    var DRAG_END = 2
    var DRAG_ALL = 3

    var mouseMode = NONE

    let longTapTimeoutId = null
    let shouldHandleCheckboxMouseUp = true



    function tryFireLongTap(e, isTouch) {
      clearTimeout(longTapTimeoutId)
      if( tryClearLongTapTimeout(e) ) { return }
      if( !(e && e.target) ) { return }
      if( !((e.which || e.keyCode) === 1 || isTouch) ) { return }

      let checkbox = e.target
      while( checkbox && checkbox.nodeName !== 'BODY' && !checkbox.classList.contains('checkbox') ) {
        checkbox = checkbox.parentNode
      }

      if( checkbox == null || checkbox.nodeName === 'BODY' ) { return }

      shouldHandleCheckboxMouseUp = false

      const inputNode = checkbox.querySelector('input')
      const columnId = inputNode.getAttribute('data-id')

      toggleCheckboxes(parseInt(columnId), null, true)
      shouldHandleCheckboxMouseUp = true
    }


    function handleIsInPreview() {
      var inPreview = (
        (mouseY.previewMover >= 0) &&
        (mouseY.previewMover <= previewHeight) &&
        (mouseX > -mouseArea) &&
        (mouseX < width + mouseArea)
      )

      if( !inPreview ) { return }

      if (mouseX > previewUiMin - mouseArea && mouseX < previewUiMin + mouseArea / 2) {
          mouseMode = DRAG_START
      } else if (mouseX > previewUiMax - mouseArea / 2 && mouseX < previewUiMax + mouseArea) {
          mouseMode = DRAG_END
      } else if (mouseX > previewUiMin + mouseArea / 2 && mouseX < previewUiMax - mouseArea / 2) {
          mouseMode = DRAG_ALL

          mouseStartX = previewUiMin - mouseX
          mouseRange = mainMaxX - mainMinX
      }
    }

    function onMouseDown(e, isTouch = false) {
        clearTimeout(longTapTimeoutId)
        longTapTimeoutId = setTimeout(function() { tryFireLongTap(e, isTouch) }, 1250)

        newMouseX = mouseX = (e.clientX - canvasBounds.main.left) * pixelRatio
        newMouseY.main = mouseY.main = (e.clientY - canvasBounds.main.top) * pixelRatio
        newMouseY.previewChart = mouseY.previewChart = (e.clientY - canvasBounds.previewChart.top) * pixelRatio
        newMouseY.previewMover = mouseY.previewMover = (e.clientY - canvasBounds.previewMover.top) * pixelRatio


        handleIsInPreview()
    }

    function onTouchDown(e) {
        onMouseDown(e.touches[0], true)
    }

    function tryClearLongTapTimeout(e) {
      if( e.target.nodeName === 'CANVAS' ) {
        clearTimeout(longTapTimeoutId)
        return true
      }

      return false
    }

    function onMouseMove(e) {
        tryClearLongTapTimeout(e)

        newMouseX = (e.clientX - canvasBounds.main.left) * pixelRatio
        newMouseY.main = (e.clientY - canvasBounds.main.top) * pixelRatio
        newMouseY.previewChart = (e.clientY - canvasBounds.previewChart.top) * pixelRatio
        newMouseY.previewMover = (e.clientY - canvasBounds.previewMover.top) * pixelRatio
    }

    function onTouchMove(e) {
        onMouseMove(e.touches[0])
    }

    function onMouseUp(e) {
        mouseMode = NONE
        clearTimeout(longTapTimeoutId)
    }

    function onMouseLeave(e) {
        mouseMode = NONE
    }


    addEventListener(container, 'mousedown', onMouseDown)
    addEventListener(container, 'touchstart', onTouchDown)
    addEventListener(document, 'mousemove', onMouseMove)
    addEventListener(document, 'touchmove', onTouchMove)
    addEventListener(container, 'mouseup', onMouseUp)
    addEventListener(container, 'mouseleave', onMouseLeave)
    addEventListener(container, 'touchend', onMouseUp)
    addEventListener(container, 'touchcancel', onMouseUp)

    var destroyed = false
    this.destroy = function () {
        destroyed = true
        removeAllChild(container)
        removeEventListener(container, 'mousedown', onMouseDown)
        removeEventListener(container, 'touchstart', onTouchDown)
        removeEventListener(document, 'mousemove', onMouseMove)
        removeEventListener(document, 'touchmove', onTouchMove)
        removeEventListener(container, 'mouseup', onMouseUp)
        removeEventListener(container, 'mouseleave', onMouseLeave)
        removeEventListener(container, 'touchend', onMouseUp)
        removeEventListener(container, 'touchcancel', onMouseUp)
    }

    window.requestAnimationFrame(render.bind(this))

    function screenToMainX(screenX) {
        return (screenX - mainOffsetX) / mainScaleX
    }

    function mainToScreenX(x, withScaleByAvgRectWidth = true) {
        let _mainToScreenX = (x * mainScaleX + mainOffsetX)

        if( withScaleByAvgRectWidth ) {
          return _mainToScreenX / mainScaleByAvgRectWidth
        }

        return _mainToScreenX
    }

    function screenToPreviewX(screenX) {
        return (screenX - previewOffsetX) / previewScaleX
    }

    function previewToScreenX(x) {
        return (x * previewScaleX + previewOffsetX) / previewScaleByAvgRectWidth
    }


    this.setTheme = function (theme) {
      if( Object.values(THEMES).indexOf(theme) > -1 ) {
        this.theme = theme
        this.isThemeLight = this.theme === THEMES.LIGHT
        this.isThemeDark = this.theme === THEMES.DARK

        needRedrawMain = needRedrawPreviewChart = needRedrawPreviewMover = true
      }
    }

    const setForceMinY = () => {
      return [KINDS.LINEAR, KINDS.LINEAR_2Y].indexOf(this.chartKind) === -1
    }

    this.setData = function (newData) {
        if( newData.y_scaled ) {
          this.setChartKind(KINDS.LINEAR_2Y)
        } else if( newData.percentage && newData.stacked ) {
          this.setChartKind(KINDS.PERCENTAGE)
        } else if( newData.stacked ) {
          this.setChartKind(KINDS.STACKED)
        } else if( newData.columns.length === 2 && Object.values(newData.types).indexOf('bar') > -1 ) {
          this.setChartKind(KINDS.BARS)
        } else {
          this.setChartKind(KINDS.LINEAR)
        }


        forceMinY = setForceMinY()

        popupLabels = []
        popupValues = []
        columns = []
        checkboxesShakeTimeouts = []

        if( this.isChartKind.percentage ) {
          popupPercentages = []
          textCountY = 4
        }

        removeAllChild(checksContainer)
        removeAllChild(popup)

        if (newData.columns.length < 2 || newData.columns[0].length < 3) {
            data = null
            return
        }

        canvases.previewChart.setAttribute('height', previewHeight)
        canvases.previewChart.style.height = `${previewHeight / pixelRatio}px`

        canvases.previewMover.setAttribute('height', previewHeight)
        canvases.previewMover.style.height = `${previewHeight / pixelRatio}px`

        const popupTitle = createElement(popup, 'div', 'title')
        popupTitleDate = createElement(popupTitle, 'div', 'date')

        data = newData
        var nameOfX = findNameOfX(data.types)

        for (var c = 0; c < data.columns.length; c++) {
            var columnData = data.columns[c]
            var name = columnData[0]
            var column = {
                name,
                title: data.names[name],
                data: columnData,
                min: name === nameOfX ? columnData[1] : (forceMinY ? 0 : Math.min(...columnData.slice(1))),
                max: name === nameOfX ? columnData[columnData.length - 1] : Math.max(...columnData.slice(1)),
                alpha: createAnimation(1, SCALE_DURATION),
                previewAlpha: createAnimation(1, SCALE_DURATION / 2),
                checked: true,
                color: data.colors[name],
            }

            if (name === nameOfX) {
                xColumn = column
            } else {
                column.id = columns.length
                columns.push(column)
            }
        }

        createCheckBoxes()
        createPopupColumns()

        intervalX = xColumn.data[2] - xColumn.data[1]
        previewMinX = xColumn.min
        previewMaxX = xColumn.max
        previewRangeX = previewMaxX - previewMinX

        onResize()
        setMainMinMax(previewMaxX - previewRangeX / 4, previewMaxX)

        // NOTE: PREVENT FIRST ANIMATION ON LOAD
        mainMinY.y0.value = mainMinY.y0.toValue
        mainMinY.y1.value = mainMinY.y1.toValue
        mainRangeY.y0.value = mainRangeY.y0.toValue
        mainRangeY.y1.value = mainRangeY.y1.toValue

        previewMinY.y0.value = previewMinY.y0.toValue
        previewMinY.y1.value = previewMinY.y1.toValue
        previewRangeY.y0.value = previewRangeY.y0.toValue
        previewRangeY.y1.value = previewRangeY.y1.toValue

        needRedrawMain = needRedrawPreviewChart = needRedrawPreviewMover = true
    }

    const forEachColumn = (callback) => {
      for (let _i = 0; _i < columns.length; _i++) {
        const column = columns[_i]
        callback(column)
      }
    }

    // NOTE: CREATE POPUP COLUMN
    const createPopupColumns = () => {
      if( this.isChartKind.percentage ) {
        const popupColumn = createElement(popup, 'div', 'column percentages')
        forEachColumn(_column => {
          const popupPercentage = createElement(popupColumn, 'div')
          popupPercentages.push(popupPercentage)
        })
      }

      let popupColumn = createElement(popup, 'div', 'column labels')
      forEachColumn(_column => {
        const popupLabel = createElement(popupColumn, 'div')
        popupLabel.innerText = _column.title
        popupLabels.push(popupLabel)
      })
      if( this.isChartKind.stacked ) {
        const popupLabel = createElement(popupColumn, 'div')
        popupLabel.innerText = 'All'
        popupLabels.push(popupLabel)
      }

      popupColumn = createElement(popup, 'div', 'column values')
      forEachColumn(_column => {
        const popupValue = createElement(popupColumn, 'div')
        popupValue.style.color = _column.color
        popupValues.push(popupValue)
      })
      if( this.isChartKind.stacked ) {
        const popupValue = createElement(popupColumn, 'div', 'total')
        popupValues.push(popupValue)
      }
    }

    function toggleCheckboxesColumns(_columns, forceChecked) {
      let changed = false
      for( let _i = 0; _i < _columns.length; _i++ ) {
        let _column = _columns[_i]

        const inputNode = checksContainer.querySelector(`input[data-id="${_column.id}"]`)
        const checkboxNode = inputNode.parentNode

        const checked = forceChecked ? true : _column.checked

        checked = !checked

        if( checked !== _column.checked ) {
          if( checked ) {
            inputNode.setAttribute('checked', 'checked')
          } else {
            inputNode.removeAttribute('checked')
          }
          inputNode.checked = checked

          _column.saveScaleY = previewScaleY
          _column.saveOffsetY = previewOffsetY
          _column.checked = checked

          play(time, _column.alpha, checked ? 1 : 0)

          _column.previewAlpha.delay = checked ? SCALE_DURATION / 2 : 0
          play(time, _column.previewAlpha, checked ? 1 : 0)

          checkboxNode.style.backgroundColor = checked ? _column.color : 'transparent'
          checkboxNode.style.color = checked ? null : _column.color

          changed = true
        }
      }

      return changed
    }

    function toggleCheckboxes(columnId, e, invert = false) {
      const checkedColumn = columns[columnId]

      let { checked } = checkedColumn

      let inputNode = checksContainer.querySelector(`input[data-id="${columnId}"]`)
      let checkboxNode = inputNode.parentNode

      let _columns
      if( checked && invert ) {
        _columns = columns.filter(_column => _column.id !== columnId && _column.checked)
      } else {
        _columns = [columns[columnId]]
      }


      let shouldShake = checked && (
        !invert && columns.filter(_column => _column.checked && _column.name !== checkedColumn.name).length === 0 ||
        _columns.length === 0
      )
      if( shouldShake ) {
        e && e.preventDefault()

        clearTimeout(checkboxesShakeTimeouts[columnId])
        checkboxNode.classList.remove('shake')
        setTimeout(() => {
          checkboxNode.classList.add('shake')

          checkboxesShakeTimeouts[columnId] = setTimeout(() => {
            checkboxNode.classList.remove('shake')
          }, 500)
        }, 0)
        return
      }

      const changed = toggleCheckboxesColumns(_columns, checked && invert)

      if( changed ) {
        let isAllUncheked = true
        for( var _i in columns ) {
          if( columns[_i].checked ) {
            isAllUncheked = false
            break
          }
        }
        forceMinY = isAllUncheked || setForceMinY()

        needRedrawMain = needRedrawPreviewChart = true
        updatePreviewRangeY()
        updateMainRangeY()
      }
    }

    // NOTE: CREATE CHECKBOX
    function createCheckBoxes() {
      if (columns.length < 2) { return }

      for( let _i = 0; _i < columns.length; _i++ ) {
        (function(_columnI) {
          const column = columns[_columnI]

          const label = createElement(checksContainer, 'label', 'checkbox')
          label.style.borderColor = column.color
          const name = column.data[0]

          var input = createElement(label, 'input')
          input.setAttribute('data-id', _columnI)
          input.checked = 'checked'
          input.setAttribute( 'checked', 'checked')
          input.type = 'checkbox'

          addEventListener(input, 'click', function (e) {
              if( !shouldHandleCheckboxMouseUp ) {
                e.preventDefault()
                shouldHandleCheckboxMouseUp = true
                return
              }
              clearTimeout(longTapTimeoutId)

              const id = e.currentTarget.getAttribute('data-id')
              toggleCheckboxes(parseInt(id), e)
          })

          let span = createElement(label, 'span', 'symbol')
          label.style.backgroundColor = column.color

          span = createElement(label, 'span', 'name')
          span.style.borderColor = column.color
          span.innerText = data.names[name]
        })(_i)
      }
    }

    const updateMainRangeX = () => {
        mainRangeX = mainMaxX - mainMinX
        mainChartWidth = width - paddingHor * 2
        mainScaleX = mainChartWidth / mainRangeX

        if( this.isChartKind.bars || this.isChartKind.stacked ) {
          const numOfBars = mainRangeX / intervalX
          const avgBarWidth = mainChartWidth / numOfBars
          mainScaleByAvgRectWidth = 1 - avgBarWidth / mainChartWidth
          mainScaleX *= mainScaleByAvgRectWidth
        }

        mainOffsetX = -mainMinX * mainScaleX + paddingHor

        var delta = mainRangeX / intervalX / textCountX

        var pow = 1
        while (pow <= delta) pow *= 2
        delta = pow

        if (delta < newTextX.delta) { // NOTE: ADD DATES
            oldTextX.delta = newTextX.delta
            oldTextX.alpha.value = 1
            play(time, oldTextX.alpha, 1)

            newTextX.delta = delta
            newTextX.alpha.value = 0
            play(time, newTextX.alpha, 1)
        } else if (delta > newTextX.delta) {  // NOTE: REMOVE DATES
            oldTextX.delta = newTextX.delta
            oldTextX.alpha.value = newTextX.alpha.value
            play(time, oldTextX.alpha, 0)

            newTextX.delta = delta
            newTextX.alpha.value = 1
            play(time, newTextX.alpha, 1)
        }
    }

    this.checkedColumns = () => {
      return columns.filter(_column => _column.checked)
    }

    // NOTE: UPDATE ANIMATED VALUES ETC
    const updateMainRangeY = () => {
        mainMaxY.y0 = forceMinY ? 0 : Number.MIN_VALUE
        if( this.isChartKind.linear2Y ) {
          mainMaxY.y1 = Number.MIN_VALUE
        }

        if( mainMinI === mainMaxI ) { return }

        let newMainMinY = {
          y0: 0,
          y1: 0,
        }
        const checkedColumns = this.checkedColumns()

        if( checkedColumns.length === 0 ) {
          mainMaxY.y0 = 0
          mainMaxY.y1 = 0
        } else if( this.isChartKind.percentage ) {
          mainMaxY.y0 = 100
        } else if ( this.isChartKind.stacked ) {
          for( let _i = mainMinI; _i < mainMaxI; _i++ ) {
            let ySum = 0
            for( let _colI = 0; _colI < columns.length; _colI++ ) {
              const _column = columns[_colI]
              if (_column.alpha.toValue === 0) { continue }

              ySum += _column.data[_i]
            }

            if( ySum > mainMaxY.y0 ) {
              mainMaxY.y0 = ySum
            }
          }
        } else {
          let minY = null,
              maxY = null

          for( let _i = 0; _i < columns.length; _i++ ) {
            const _column = columns[_i]
            if (_column.alpha.toValue === 0) { continue }

            const _selectedDataY = _column.data.slice(mainMinI, mainMaxI)
            const _yMin = forceMinY ? 0 : Math.min(..._selectedDataY)
            const _yMax = Math.max(..._selectedDataY)

            if( this.isChartKind.linear2Y ) {
              newMainMinY[ _i === 0 ? 'y0' : 'y1' ] = _yMin
              mainMaxY[ _i === 0 ? 'y0' : 'y1' ] = _yMax
            } else {
              if ( minY == null || minY > _yMin ) { minY = _yMin }
              if( maxY == null || maxY < _yMax ) { maxY = _yMax }
            }
          }

          if( this.isChartKind.linear1Y || this.isChartKind.bars ) {
            newMainMinY.y0 = minY
            mainMaxY.y0 = maxY
          }
        }

        if( mainMinY.y0.toValue !== newMainMinY.y0 ) {
          play(time, mainMinY.y0, newMainMinY.y0)
        }
        if( this.isChartKind.linear2Y && mainMinY.y1.toValue !== newMainMinY.y1 ) {
          play(time, mainMinY.y1, newMainMinY.y1)
        }


        for( let _i = 0; _i < yAxisNames.length; _i++ ) {
          const _yName = yAxisNames[_i]

          if( !this.isChartKind.linear2Y && _i > 0 ) { break }

          if (mainMaxY[_yName] === Number.MIN_VALUE) { mainMaxY[_yName] = 1 }

          let range = mainMaxY[_yName] - mainMinY[_yName].toValue
          if( range < 0 ) { range = 0 }

          if (mainRangeY[_yName].toValue !== range) {
              play(time, mainRangeY[_yName], range)

              oldTextY[_yName].delta = newTextY[_yName].delta
              oldTextY[_yName].alpha.value = newTextY[_yName].alpha.value
              play(time, oldTextY[_yName].alpha, 0)

              newTextY[_yName].delta = Math.floor(mainRangeY[_yName].toValue / textCountY)
              newTextY[_yName].alpha.value = 1 - oldTextY[_yName].alpha.value
              play(time, newTextY[_yName].alpha, 1)
          }
        }
    }

    const updatePreviewRangeX = () => {
        previewChartWidth = width - paddingHor * 2
        previewScaleX = previewChartWidth / previewRangeX

        if( this.isChartKind.bars || this.isChartKind.stacked ) {
          const numOfBars = previewRangeX / intervalX
          const avgBarWidth = previewChartWidth / numOfBars
          previewScaleByAvgRectWidth = 1 - avgBarWidth / previewChartWidth
          previewScaleX *= previewScaleByAvgRectWidth
        }

        previewOffsetX = -previewMinX * previewScaleX + paddingHor
    }

    const updatePreviewRangeY = () => {
        previewMaxY.y0 = forceMinY ? 0 : Number.MIN_VALUE
        if( this.isChartKind.linear2Y ) {
          previewMaxY.y1 = Number.MIN_VALUE
        }

        const checkedColumns = this.checkedColumns()

        let newPreviewMinY = {
          y0: 0,
          y1: 0,
        }

        if( checkedColumns.length === 0 ) {
          newPreviewMinY.y0 = 0
          newPreviewMinY.y1 = 0

          previewMaxY.y0 = 0
          previewMaxY.y1 = 0
        } if( this.isChartKind.percentage ) {
          previewMaxY.y0 = 100
        } else if ( this.isChartKind.stacked ) {
          for( let _i = 1; _i < columns[0].data.length; _i++ ) {
            let ySum = 0
            for( let _colI = 0; _colI < columns.length; _colI++ ) {
              const _column = columns[_colI]
              if (_column.previewAlpha.toValue === 0) { continue }

              ySum += _column.data[_i]
            }

            if( ySum > previewMaxY.y0 ) {
              previewMaxY.y0 = ySum
            }
          }
        } else {
          let minY = null,
              maxY = null

          for( let _i = 0; _i < columns.length; _i++ ) {
            const _column = columns[_i]

            if (_column.previewAlpha.toValue === 0) continue

            const _yMin = forceMinY ? 0 : _column.min
            const _yMax = _column.max

            if( this.isChartKind.linear2Y ) {
              newPreviewMinY[ _i === 0 ? 'y0' : 'y1' ] = _yMin
              previewMaxY[ _i === 0 ? 'y0' : 'y1' ] = _yMax
            } else {
              if ( minY == null || minY > _yMin ) { minY = _yMin }
              if( maxY == null || maxY < _yMax ) { maxY = _yMax }
            }
          }

          if( this.isChartKind.linear1Y || this.isChartKind.bars ) {
            newPreviewMinY.y0 = minY
            previewMaxY.y0 = maxY
          }
        }

        if (previewMaxY.y0 === Number.MIN_VALUE) { previewMaxY.y0 = 1 }

        for( let _i = 0; _i < yAxisNames.length; _i++ ) {
          if( !this.isChartKind.linear2Y && _i > 0 ) { break }

          const _yName = yAxisNames[_i]

          if( previewMinY[_yName].toValue !== newPreviewMinY[_yName] ) {
            play(time, previewMinY[_yName], newPreviewMinY[_yName])
          }

          const newPreviewRangeY = previewMaxY[_yName] - previewMinY[_yName].toValue
          if( previewRangeY[_yName].toValue !== newPreviewRangeY ) {
            play(time, previewRangeY[_yName], newPreviewRangeY)
          }
        }
    }

    function setMainMinMax(min, max) {
        let changedMin = false,
            changedMax = false

        if (min !== null && mainMinX !== min) {
            mainMinX = min
            changedMin = true
        }

        if (max !== null && mainMaxX !== max) {
            mainMaxX = max
            changedMax = true
        }

        if( changedMin || changedMax ) {
          updateMainRangeX()

          if( changedMin ) {
            mainMinI = Math.floor((mainMinX - previewMinX - paddingHor / mainScaleX) / intervalX) + 1
            if (mainMinI < 1) mainMinI = 1
          }

          if( changedMax ) {
            mainMaxI = Math.ceil((mainMaxX - previewMinX + paddingHor / mainScaleX) / intervalX) + 2
            if (mainMaxI > xColumn.data.length) mainMaxI = xColumn.data.length
          }

          updateMainRangeY()

          needRedrawMain = needRedrawPreviewMover = true
        }
    }

    const select = (mouseX, mouseY) => {
        if (selectX !== mouseX) {
            selectX = mouseX
            needRedrawMain = true

            if (selectX === null) {
                selectI = -1
                popup.style.display = 'none'
            } else {
                popup.style.display = 'block'

                const roundMathFunc = (this.isChartKind.bars || this.isChartKind.stacked) ? 'floor' : 'round'

                var newSelectI = Math[roundMathFunc]((mouseX - previewMinX) / intervalX) + 1
                if (newSelectI < 1) {newSelectI = 1}
                if (newSelectI > xColumn.data.length - 1) {newSelectI = xColumn.data.length - 1}

                if (selectI !== newSelectI) {
                    selectI = newSelectI
                    var x = xColumn.data[selectI]
                    popupTitleDate.innerText = formatDate(x, false)

                    let total = 0
                    for (var c = 0; c < columns.length; c++) {
                        var yColumn = columns[c]
                        var y = yColumn.data[selectI]
                        popupLabels[c].style.display = yColumn.alpha.toValue === 0 ? 'none' : 'block'

                        popupValues[c].style.display = yColumn.alpha.toValue === 0 ? 'none' : 'block'
                        popupValues[c].innerText = formatNumber(y, false)

                        if( yColumn.checked ) {
                          total += y
                        }
                    }

                    if( this.isChartKind.stacked ) {
                      popupValues[ popupValues.length - 1 ].innerText = formatNumber(total, false)
                    }

                    if( this.isChartKind.percentage ) {
                      for (var c = 0; c < columns.length; c++) {
                          var yColumn = columns[c]
                          var y = yColumn.data[selectI]
                          popupPercentages[c].style.display = yColumn.alpha.toValue === 0 ? 'none' : 'block'
                          popupPercentages[c].innerText = formatNumber(y / total * 100, false).trim() + '%'
                      }
                    }
                }

                var popupBounds = popup.getBoundingClientRect()
                const _w = (mainToScreenX(mouseX) / pixelRatio)
                var popupX = _w + popupLeftMargin


                const maxAllowedLeftX = popupTextYWidth
                if( _w < maxAllowedLeftX ) {
                  popupX = maxAllowedLeftX + popupLeftMargin
                }

                let maxAllowedRightX = width / pixelRatio - popupBounds.width - popupLeftMargin
                if( this.isChartKind.linear2Y ) {
                  maxAllowedRightX -= popupTextYWidth  - paddingHor
                }

                if( _w > maxAllowedRightX ) {
                  popupX = maxAllowedRightX + popupLeftMargin
                }

                if( _w > maxAllowedRightX - popupLeftMargin ) {
                  const _popupXLeft = _w - popupBounds.width - popupLeftMargin
                  if( _popupXLeft >= maxAllowedLeftX ) {
                    popupX = _popupXLeft
                  }
                }

                popup.style.left = popupX + 'px'
            }
        }

        if (selectY !== mouseY) {
            selectY = mouseY
            if (!popupBounds) popupBounds = popup.getBoundingClientRect()
            var popupY = popupTopMargin
            popup.style.top = popupY + 'px'
        }
    }

    const onResizeMainCanvas = () => {
      const _canvasBounds = canvases.main.getBoundingClientRect()

      canvasBounds.main.width = _canvasBounds.width
      canvasBounds.main.height = _canvasBounds.height
      canvasBounds.main.top = _canvasBounds.top
      canvasBounds.main.left = _canvasBounds.left

      var newWidth = canvasBounds.main.width * pixelRatio
      var newHeight = canvasBounds.main.height * pixelRatio

      if (width !== newWidth || height !== newHeight) {
          width = newWidth
          height = newHeight
          mainHeight = height - textXMargin - mainPaddingBottom
          textCountX = Math.max(1, Math.floor(width / (textXWidth * 2)))
          if( !this.isChartKind.percentage ) {
            textCountY = Math.max(1, Math.floor(mainHeight / textYHeight))
          }

          canvases.main.setAttribute('width', width)
          canvases.main.setAttribute('height', height)
          updateMainRangeX()
          updateMainRangeY()

          // TODO: UPD IN "onResizeCanvasPreviewMover"
          updatePreviewRangeX()
          updatePreviewRangeY()

          // TODO: UPD "needRedrawPreviewChart" IN "onResizeCanvasPreviewMover"
          needRedrawMain = needRedrawPreviewChart = true
      }
    }

    const onResizeCanvasPreviewChart = () => {
      const _canvasBounds = canvases.previewChart.getBoundingClientRect()

      const oldWidth = canvasBounds.previewChart.width * pixelRatio

      canvasBounds.previewChart.width = _canvasBounds.width
      canvasBounds.previewChart.height = _canvasBounds.height
      canvasBounds.previewChart.top = _canvasBounds.top
      canvasBounds.previewChart.left = _canvasBounds.left

      // TODO: PREVENT MULTIPLY ON EACH RENDER
      const newWidth = canvasBounds.previewChart.width * pixelRatio

      if (newWidth !== oldWidth) {
          canvases.previewChart.setAttribute('width', newWidth)
          needRedrawPreviewChart = true
      }
    }

    const onResizeCanvasPreviewMover = () => {
      const _canvasBounds = canvases.previewMover.getBoundingClientRect()

      const oldWidth = canvasBounds.previewMover.width * pixelRatio

      canvasBounds.previewMover.width = _canvasBounds.width
      canvasBounds.previewMover.height = _canvasBounds.height
      canvasBounds.previewMover.top = _canvasBounds.top
      canvasBounds.previewMover.left = _canvasBounds.left

      // TODO: PREVENT MULTIPLY ON EACH RENDER
      const newWidth = canvasBounds.previewMover.width * pixelRatio

      if (newWidth !== oldWidth) {
          canvases.previewMover.setAttribute('width', newWidth)
          needRedrawPreviewMover = true
      }
    }

    const onResize = () => {
        onResizeMainCanvas()
        onResizeCanvasPreviewChart()
        onResizeCanvasPreviewMover()
    }

    function render(t) {
        if (destroyed) return
        time = t

        if (data !== null) {

            // NOTE: RESIZE

            onResize()

            if (width > 0 && height > 0) {
                // NOTE: MOUSE

                if (mouseMode > 0) {
                    mouseX += (newMouseX - mouseX) * 0.5
                    mouseY.main += (newMouseY.main - mouseY.main) * 0.5
                    mouseY.previewChart += (newMouseY.previewChart - mouseY.previewChart) * 0.5
                    mouseY.previewMover += (newMouseY.previewMover - mouseY.previewMover) * 0.5
                } else {
                    mouseX = newMouseX
                    mouseY = newMouseY
                }

                if (mouseMode === DRAG_START) {
                    var x = mouseX
                    if (x > previewUiMax - mouseArea * 2) x = previewUiMax - mouseArea * 2
                    var newMinX = screenToPreviewX(x)
                    if (newMinX < previewMinX) newMinX = previewMinX
                    setMainMinMax(newMinX, null)
                } else if (mouseMode === DRAG_END) {
                    var x = mouseX
                    if (x < previewUiMin + mouseArea * 2) x = previewUiMin + mouseArea * 2
                    var newMaxX = screenToPreviewX(x)
                    if (newMaxX > previewMaxX) newMaxX = previewMaxX
                    setMainMinMax(null, newMaxX)
                } else if (mouseMode === DRAG_ALL) {
                    var startX = mouseX + mouseStartX
                    var newMinX = screenToPreviewX(startX)
                    if (newMinX < previewMinX) newMinX = previewMinX
                    if (newMinX > previewMaxX - mouseRange) newMinX = previewMaxX - mouseRange
                    setMainMinMax(newMinX, newMinX + mouseRange)
                }

                var inMain = (
                    (mouseY.main > 0) &&
                    (mouseY.main < height) &&
                    (mouseX > 0) &&
                    (mouseX < width)
                )
                if (inMain) {
                    select(screenToMainX(Math.floor(mouseX)), Math.floor(mouseY.main))
                } else {
                    select(null, null)
                }

                // NOTE: ANIMATION

                if (updateAnimation(time, oldTextX.alpha)) { needRedrawMain = true }
                if (updateAnimation(time, newTextX.alpha)) { needRedrawMain = true }

                if (updateAnimation(time, oldTextY.y0.alpha)) { needRedrawMain = true }
                if (this.isChartKind.linear2Y && updateAnimation(time, oldTextY.y1.alpha)) { needRedrawMain = true }

                if (updateAnimation(time, newTextY.y0.alpha)) { needRedrawMain = true }
                if (this.isChartKind.linear2Y && updateAnimation(time, newTextY.y1.alpha)) { needRedrawMain = true }

                if (updateAnimation(time, mainRangeY.y0)) { needRedrawMain = true }
                if (this.isChartKind.linear2Y && updateAnimation(time, mainRangeY.y1)) { needRedrawMain = true }

                if (updateAnimation(time, mainMinY.y0)) { needRedrawMain = true }
                if (this.isChartKind.linear2Y && updateAnimation(time, mainMinY.y1)) { needRedrawMain = true }

                if (updateAnimation(time, previewRangeY.y0)) { needRedrawPreviewChart = true }
                if (this.isChartKind.linear2Y && updateAnimation(time, previewRangeY.y1)) { needRedrawPreviewChart = true }

                if (updateAnimation(time, previewMinY.y0)) { needRedrawPreviewChart = true }
                if (this.isChartKind.linear2Y && updateAnimation(time, previewMinY.y1)) { needRedrawPreviewChart = true }

                for (var c = 0; c < columns.length; c++) {
                    var yColumn = columns[c]
                    if (updateAnimation(time, yColumn.alpha)) { needRedrawMain = true }
                    if (updateAnimation(time, yColumn.previewAlpha)) { needRedrawPreviewChart = true }
                }

                // NOTE: RENDER

                if (needRedrawMain) {
                    needRedrawMain = false
                    renderMain()
                }
                if (needRedrawPreviewChart) {
                    needRedrawPreviewChart = false
                    renderPreviewChart()
                }
                if (needRedrawPreviewMover) {
                    needRedrawPreviewMover = false
                    renderPreviewMover()
                }

            }
        }

        window.requestAnimationFrame(render.bind(this))
    }

    function renderTextsX(textX, skipStep) {
        if (textX.alpha.value === 0) {
          return
        }

        const ctx = contexts.main

        ctx.globalAlpha = textX.alpha.value
        ctx.textBaseline = 'bottom'

        var delta = textX.delta
        if (skipStep) delta *= 2

        var endI = Math.min(Math.ceil(mainMaxX / intervalX / delta) * delta, xColumn.data.length)
        if (skipStep) endI -= textX.delta
        var startI = Math.max(mainMinI - 1, 1)

        for (var i = endI - 1; i >= startI; i -= delta) {
            var value = xColumn.data[i]
            var x = mainToScreenX(value)
            var offsetX = 0
            if (i === xColumn.data.length - 1) {
                offsetX = -textXWidth
            } else if (i > 1) {
                offsetX = -(textXWidth / 2)
            }
            ctx.fillText(formatDate(value, true), x + offsetX, mainHeight + textXMargin)
        }
    }




    function renderPreviewRadius(ctx, startX, startY, borderKind) {
        // NOTE: ROUND WHOLE CHART - TOP LEFT
        ctx.beginPath()
        ctx.moveTo(
          startX,
          startY + ((borderKind === 'bottomLeft' ||  borderKind === 'bottomRight') ? -previewBorderRadius : previewBorderRadius)
        )
        ctx.lineTo(
          startX,
          startY
        )
        ctx.lineTo(
          startX + ((borderKind === 'topRight' || borderKind === 'bottomRight') ? -previewBorderRadius : previewBorderRadius),
          startY
        )
        ctx.quadraticCurveTo(
          startX,
          startY,
          startX,
          startY + ((borderKind === 'bottomLeft' ||  borderKind === 'bottomRight') ? -previewBorderRadius : previewBorderRadius)
        )
        ctx.fill()
    }

    // NOTE: DRAW PREVIEW UI
    const renderPreviewMover = () => {
      const ctx = contexts.previewMover
      ctx.clearRect(0, 0, width, previewHeight)

      previewUiMin = previewToScreenX(mainMinX)
      previewUiMax = Math.floor(previewToScreenX(mainMaxX))

      // NOTE: PREVIEW SEMI-TRANSPARENT BG
      ctx.globalAlpha = THEMES_COLORS[this.theme].previewAlpha
      ctx.fillStyle = THEMES_COLORS[this.theme].preview
      // NOTE: LEFT HORIZONTAL SEMI-TRANSPARENT RECT
      roundRect(
        ctx,
        paddingHor,
        previewUiH,
        previewUiMin - paddingHor + previewUiW / 2,
        previewChartH,
        { tl: previewUiRadius, tr: 0, bl: previewUiRadius, br: 0 },
      )
      // NOTE: RIGHT HORIZONTAL SEMI-TRANSPARENT RECT
      roundRect(
        ctx,
        previewUiMax - previewUiW / 2,
        previewUiH,
        width - previewUiMax - paddingHor + previewUiW / 2,
        previewChartH,
        { tl: 0, tr: previewUiRadius, bl: 0, br: previewUiRadius }
      )


      // NOTE: PREVIEW MOVING STRIPE
      ctx.globalAlpha = THEMES_COLORS[this.theme].previewBorderAlpha
      ctx.fillStyle = THEMES_COLORS[this.theme].previewBorder

      // NOTE: LEFT
      roundRect(
        ctx,
        previewUiMin,
        0,
        previewUiW,
        previewHeight,
        { tl: previewUiRadius, tr: 0, bl: previewUiRadius, br: 0 }
      )

      // NOTE: RIGHT ROUNDED TOUCH
      roundRect(
        ctx,
        previewUiMax - previewUiW,
        0,
        previewUiW,
        previewHeight,
        { tl: 0, tr: previewUiRadius, bl: 0, br: previewUiRadius }
      )

      ctx.beginPath()
      // NOTE: TOP
      ctx.rect(previewUiMin + previewUiW, 0, previewUiMax - previewUiMin - previewUiW * 2, previewUiH)
      // NOTE: DOWN
      ctx.rect(previewUiMin + previewUiW, previewHeight - previewUiH, previewUiMax - previewUiMin - previewUiW * 2, previewUiH)

      ctx.fill()

      // NOTE: LEFT AND RIGHT- TOUCH LINE
      ctx.globalAlpha = THEMES_COLORS[this.theme].previewUiWTouchAlpha
      ctx.fillStyle = THEMES_COLORS[this.theme].previewUiWTouchLine

      // NOTE: LEFT- TOUCH LINE
      roundRect(
        ctx,
        previewUiMin + previewUiW / 2 - previewUiWTouchLineW / 2,
        previewHeight / 2 - previewUiWTouchLineH / 2,
        previewUiWTouchLineW,
        previewUiWTouchLineH,
        previewUiWTouchLineRadius
      )

      // NOTE: RIGHT - TOUCH LINE
      roundRect(
        ctx,
        previewUiMax - previewUiW / 2 - previewUiWTouchLineW / 2,
        previewHeight / 2 - previewUiWTouchLineH / 2,
        previewUiWTouchLineW,
        previewUiWTouchLineH,
        previewUiWTouchLineRadius
      )


      if( this.isThemeLight ) {
        ctx.globalAlpha = 1
        ctx.fillStyle = WHITE_COLOR
        ctx.beginPath()

        const _insideLineStartY = previewUiH
        const _insideLineWidth = pixelRatio
        const _insideLineHeight = previewHeight - previewUiH * 2

        // NOTE: LEFT WHITE LINE INSIDE
        ctx.rect(
          previewUiMin + previewUiW,
          _insideLineStartY,
          _insideLineWidth,
          _insideLineHeight
        )
        // NOTE: RIGHT WHITE LINE INSIDE
        ctx.rect(
          previewUiMax - previewUiW - _insideLineWidth,
          _insideLineStartY,
          _insideLineWidth,
          _insideLineHeight
        )

        ctx.fill()

        ctx.strokeStyle = THEMES_COLORS[this.theme].previewUiWTouchLine
        ctx.lineWidth = pixelRatio

        // NOTE: LEFT WHITE LINE OUTSIDE
        ctx.beginPath()

        let _x = previewUiMin
        const _y = 0
        let _xRight = _x + previewUiW / 2

        ctx.moveTo(_xRight, _y)
        ctx.quadraticCurveTo(_x, _y, _x, _y + previewUiW)
        ctx.lineTo(_x, previewHeight - previewUiW)
        ctx.quadraticCurveTo(_x, previewHeight, _xRight, previewHeight)

        ctx.stroke()

        // NOTE: RIGHT WHITE LINE OUTSIDE
        ctx.beginPath()

        _x = previewUiMax - previewUiW / 2
        _xRight = previewUiMax

        ctx.moveTo(_x, _y)
        ctx.quadraticCurveTo(_xRight, _y, _xRight, _y + previewUiW)
        ctx.lineTo(_xRight, previewHeight - previewUiW)
        ctx.quadraticCurveTo(_xRight, previewHeight, _x, previewHeight)

        ctx.stroke()
      }
    }

    const renderPreviewChart = () => {
      const ctx = contexts.previewChart

      ctx.clearRect(0, 0, width, previewHeight)

      // NOTE: CHART - PATHS
      this.forEachAxisY(_yName => {
        previewScaleY[_yName] = (-previewChartH + ( this.isChartKind.stacked ? 0 : previewUiH)) / previewRangeY[_yName].value
        previewOffsetY[_yName] = previewChartH - previewMinY[_yName].value * previewScaleY[_yName]
      })

      if( this.isChartKind.stacked ) {
        renderStacked(
          ctx,
          width - paddingHor * 2,
          columns,
          1,
          columns[0].data.length,
          xColumn,

          previewScaleX,
          previewOffsetX,

          previewScaleY.y0,
          previewOffsetY.y0,

          previewChartH,
          previewUiH,
          pixelRatio,
          true
        )
      } else if( this.isChartKind.percentage ) {
        renderPercentage(
          ctx,
          width,
          columns,
          1,
          columns[0].data.length,
          paddingHor,
          xColumn,

          previewScaleX,
          previewOffsetX,

          previewScaleY.y0,
          previewOffsetY.y0,

          previewChartH,
          previewUiH,
          true
        )
      } else if( this.isChartKind.bars ) {
        for (var _i = 0; _i < columns.length; _i++) {
            var yColumn = columns[_i]

            if (yColumn.previewAlpha.value === 0) continue

            var columnScaleY = previewScaleY.y0
            var columnOffsetY = previewOffsetY.y0

            if (yColumn.alpha.toValue === 0) {
                columnScaleY = yColumn.saveScaleY.y0
                columnOffsetY = yColumn.saveOffsetY.y0
            }

            renderBars(
              ctx,
              width - paddingHor * 2,
              xColumn,
              yColumn,
              1,
              yColumn.data.length,
              previewScaleX,
              columnScaleY,
              previewOffsetX,
              columnOffsetY,
              paddingHor,
              yColumn.previewAlpha.value,
              this.chartKind,
              previewChartH + previewUiH,
            )
        }
      } else {
        for (var _i = 0; _i < columns.length; _i++) {
            var yColumn = columns[_i]
            const _yName = this.isChartKind.linear2Y && _i > 0 ? 'y1' : 'y0'

            if (yColumn.previewAlpha.value === 0) continue

            var columnScaleY = previewScaleY[_yName]
            var columnOffsetY = previewOffsetY[_yName]

            if (yColumn.alpha.toValue === 0) {
                columnScaleY = yColumn.saveScaleY[_yName]
                columnOffsetY = yColumn.saveOffsetY[_yName]
            }

            // TOREMOVE (?): CAUSES FLICKER WHEN TOGGLE FIRST COLUMN ON 1Y/2Y CHARTS
            // } else {
            //     const columnRangeY = yColumn.max - yColumn.min
            //     if (columnRangeY > previewRangeY[_yName].value) {
            //         columnScaleY = -previewChartH / columnRangeY
            //         columnOffsetY = height - previewMinY[_yName].value * columnScaleY
            //     }
            // }

            ctx.lineWidth = previewLineWidth


            renderPath(
              ctx,
              width - paddingHor * 2,
              xColumn,
              yColumn,
              1,
              yColumn.data.length,
              previewScaleX,
              columnScaleY,
              previewOffsetX,
              columnOffsetY,
              yColumn.previewAlpha.value,
            )
        }
      }

      // NOTE: ROUND WHOLE CHART
      ctx.save()
      ctx.globalAlpha = 1
      ctx.fillStyle = THEMES_COLORS[this.theme].background
      ctx.lineWidth = pixelRatio
      ctx.globalCompositeOperation = "xor"

      let previewStartX = paddingHor
      let previewStartY = previewUiH + previewChartH

      // NOTE: ROUND WHOLE CHART - TOP LEFT
      renderPreviewRadius(ctx, previewStartX, previewUiH, 'topLeft')

      // NOTE: ROUND WHOLE CHART - BOTTOM LEFT
      renderPreviewRadius(ctx, previewStartX, previewStartY, 'bottomLeft')

      previewStartX = width - paddingHor
      // NOTE: ROUND WHOLE CHART - TOP RIGHT
      renderPreviewRadius(ctx, previewStartX, previewUiH, 'topRight')

      // NOTE: ROUND WHOLE CHART - BOTTOM RIGHT
      renderPreviewRadius(ctx, previewStartX, previewStartY, 'bottomRight')

      ctx.restore()
    }


    this.forEachAxisY = (callback) => {
      for( let _i = 0; _i < yAxisNames.length; _i++ ) {
        const _yName = yAxisNames[_i]
        callback(_yName)
      }
    }

    function getMainChartWidthWithOffsets() {
        const firstBarStartX = Math.floor(xColumn.data[mainMinI] * mainScaleX + mainOffsetX)
        const leftPadding = firstBarStartX < 0 ? paddingHor : ( paddingHor - firstBarStartX)
        if( leftPadding > paddingHor ) { leftPadding = paddingHor }

        let rightPoint = mainToScreenX(xColumn.data[mainMaxI - 1])
        if( rightPoint > width ) { rightPoint = width }

        let rightPadding = Math.floor(rightPoint - (width - paddingHor))
        if( rightPadding > paddingHor ) { rightPadding = paddingHor }
        rightPadding = paddingHor - rightPadding


        const mainChartWidth = Math.floor(width - leftPadding - rightPadding)

        return ({
          mainChartWidth,
          leftPadding,
          rightPadding,
        })
    }

    // NOTE: RENDER MAIN CHART
    const renderMain = () => {
        const ctx = contexts.main

        ctx.clearRect(0, 0, width, mainHeight + textXMargin + mainPaddingBottom)

        let mainScaleY = {
          y0: 1,
          y1: 1,
        }
        let mainOffsetY = {
          y0: 0,
          y1: 0,
        }
        const checkedColumns = this.checkedColumns()

        this.forEachAxisY(_yName => {
          mainScaleY[_yName] = -(mainHeight - mainPaddingTop) / mainRangeY[_yName].value
          mainOffsetY[_yName] = mainHeight - mainMinY[_yName].value * mainScaleY[_yName]
        })

        // NOTE: PATHS

        if( this.isChartKind.stacked ) {
          const {
            leftPadding,
            rightPadding,
          } = getMainChartWidthWithOffsets()


          renderStacked(
            ctx,
            width - paddingHor,
            columns,
            mainMinI,
            mainMaxI,
            xColumn,

            mainScaleX,
            mainOffsetX,

            mainScaleY.y0,
            mainOffsetY.y0,

            mainHeight,
            0,
            pixelRatio,
            false,
            selectI,
            leftPadding - rightPadding
          )
        } else if( this.isChartKind.percentage ) {
          renderPercentage(
            ctx,
            width,
            columns,
            mainMinI,
            mainMaxI,
            paddingHor,
            xColumn,

            mainScaleX,
            mainOffsetX,

            mainScaleY.y0,
            mainOffsetY.y0,

            mainHeight - mainPaddingTop,
            mainPaddingTop,
            false,
            selectX
          )
        } else if( this.isChartKind.bars ) {
          const {
            leftPadding,
            rightPadding,
          } = getMainChartWidthWithOffsets()

          for (var _i = 0; _i < columns.length; _i++) {
              var yColumn = columns[_i]

              if (yColumn.alpha.value === 0) continue

              renderBars(
                  ctx,
                  width - paddingHor,
                  xColumn,
                  yColumn,
                  mainMinI,
                  mainMaxI,
                  mainScaleX,
                  mainScaleY.y0,
                  mainOffsetX,
                  mainOffsetY.y0,
                  paddingHor,
                  yColumn.alpha.value,
                  this.chartKind,
                  mainHeight,
                  selectI,
                  leftPadding - rightPadding
              )
          }
        } else {
          for (var _i = 0; _i < columns.length; _i++) {
              var yColumn = columns[_i]

              if (yColumn.alpha.value === 0) continue

              ctx.lineWidth = mainLineWidth

              const _yName = this.isChartKind.linear2Y && _i > 0 ? 'y1' : 'y0'

              renderPath(
                ctx,
                width - paddingHor,
                xColumn,
                yColumn,
                mainMinI,
                mainMaxI,
                mainScaleX,
                mainScaleY[_yName],
                mainOffsetX,
                mainOffsetY[_yName],
                yColumn.alpha.value
              )
          }
        }


        // NOTE: LINES

        ctx.strokeStyle = THEMES_COLORS[this.theme].line
        ctx.lineWidth = lineWidth

        // NOTE: DISABLE MOVING OF X-LINES WHEN 1 LINE CHOSED
        if( this.isChartKind.linear2Y && checkedColumns.length > 1 ) {
          // TODO: CACHE CALCS, {alpha...}
          const __scale = -(mainHeight - mainPaddingTop)/mainHeight
          const __offset = mainHeight
          renderLinesY(ctx, { alpha: {value: 1}, delta: Math.floor(mainHeight / textCountY)}, textCountY, mainMinYDefault, __scale, __offset, paddingHor, width)
        } else {
          const yName = this.isChartKind.linear2Y ? checkedColumns[0].name : 'y0'
          renderLinesY(ctx, oldTextY[yName], textCountY, mainMinY[yName], mainScaleY[yName], mainOffsetY[yName], paddingHor, width, this.isChartKind.percentage)
          renderLinesY(ctx, newTextY[yName], textCountY, mainMinY[yName], mainScaleY[yName], mainOffsetY[yName], paddingHor, width, this.isChartKind.percentage)
        }

        ctx.globalAlpha = 1
        ctx.strokeStyle = THEMES_COLORS[this.theme].zeroLine
        ctx.beginPath()
        ctx.moveTo(paddingHor, mainHeight)
        ctx.lineTo(width - paddingHor, mainHeight)
        ctx.stroke()


        // NOTE: SELECT ON HOVER - CIRCLES/FADE/ETC
        if (selectX) {
            ctx.globalAlpha = 1
            ctx.strokeStyle = THEMES_COLORS[this.theme].selectLine
            ctx.lineWidth = lineWidth
            ctx.beginPath()
            let x = mainToScreenX(selectX, false)
            ctx.moveTo(x, 0)
            ctx.lineTo(x, mainHeight)
            ctx.stroke()


            if( this.isChartKind.linear1Y || this.isChartKind.linear2Y ) {
              x = xColumn.data[selectI]
              for (var _i = 0; _i < columns.length; _i++) {
                  const _yName = this.isChartKind.linear2Y && _i > 0 ? 'y1' : 'y0'

                  var yColumn = columns[_i]
                  if (yColumn.alpha.toValue === 0) continue

                  var y = yColumn.data[selectI]
                  ctx.strokeStyle = yColumn.color
                  ctx.fillStyle = THEMES_COLORS[this.theme].circleFill
                  ctx.lineWidth = circleLineWidth

                  ctx.beginPath()
                  ctx.arc(
                    x * mainScaleX + mainOffsetX,
                    y * mainScaleY[_yName] + mainOffsetY[_yName],
                    circleRadius,
                    0,
                    Math.PI * 2
                  )
                  ctx.stroke()
                  ctx.fill()
              }
            }
        }

        // NOTE: TEXT X
        ctx.font = font
        ctx.textAlign = 'start'
        ctx.fillStyle = THEMES_COLORS[this.theme].text

        var skipStepNew = oldTextX.delta > newTextX.delta
        renderTextsX(oldTextX, !skipStepNew)
        renderTextsX(newTextX, skipStepNew)

        // NOTE: TEXT LEFT Y
        if( this.isChartKind.linear2Y ) {
          ctx.fillStyle = columns[0].color
        }

        renderTextsY(ctx, oldTextY.y0, textCountY, mainScaleY.y0, mainOffsetY.y0, paddingHor, textYMargin, mainMinY.y0, false, this.isChartKind.percentage)
        renderTextsY(ctx, newTextY.y0, textCountY, mainScaleY.y0, mainOffsetY.y0, paddingHor, textYMargin, mainMinY.y0, this.isChartKind.linear2Y && !columns[0].checked, this.isChartKind.percentage)

        // NOTE: TEXT LEFT Y - BASE
        ctx.globalAlpha = oldTextY.y0.alpha.value
        ctx.fillText(formatNumber(mainMinY.y0.value, true), paddingHor, mainHeight + textYMargin)
        ctx.globalAlpha = (!this.isChartKind.linear2Y || columns[0].checked) ? newTextY.y0.alpha.value : (1 - newTextY.y0.alpha.value)
        ctx.fillText(formatNumber(mainMinY.y0.value, true), paddingHor, mainHeight + textYMargin)

        // // NOTE: TEXT RIGHT Y
        if( this.isChartKind.linear2Y ) {
          ctx.fillStyle = columns[1].color
          ctx.textAlign = 'end'
          renderTextsY(ctx, oldTextY.y1, textCountY, mainScaleY.y1, mainOffsetY.y1, width - paddingHor, textYMargin, mainMinY.y1)
          renderTextsY(ctx, newTextY.y1, textCountY, mainScaleY.y1, mainOffsetY.y1, width - paddingHor, textYMargin, mainMinY.y1, !columns[1].checked)
          // NOTE: TEXT RIGHT Y - BASE
          ctx.globalAlpha = columns[1].checked ? newTextY.y1.alpha.value : (1 - newTextY.y1.alpha.value)
          ctx.fillText(formatNumber(mainMinY.y1.value, true), width - paddingHor, mainHeight + textYMargin)
        }

        // NOTE: DATE RANGE
        // TODO: ADD ANIMATION - OLD TEXT GOES UP GROWS DOWN AND FADE OUT, NEW FADE IN AND GROWS UP FROM BOTTOM
        datesRangesTitle.innerText = `${formatDate(xColumn.data[mainMinI], false, true)} - ${formatDate(xColumn.data[mainMaxI - 1], false, true)}`
    }
}

TgChart.THEMES = THEMES
TgChart.THEMES_COLORS = THEMES_COLORS
TgChart.KINDS = KINDS
