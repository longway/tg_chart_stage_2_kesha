export default function updateAnimation(time, anim) {
    if (anim.value === anim.toValue) { return false }

    let progress = ((time - anim.startTime) - anim.delay) / anim.duration
    if (progress < 0) progress = 0
    if (progress > 1) progress = 1
    var ease = -progress * (progress - 2)
    anim.value = anim.fromValue + (anim.toValue - anim.fromValue) * ease

    return true
}
