'use strict'

gulp = require('gulp')
source = require('vinyl-source-stream')
buffer = require('vinyl-buffer')
size = require('gulp-size')
gulpif = require('gulp-if')
concat = require('gulp-concat')
replace = require('gulp-replace')
classPrefix = require('gulp-class-prefix')
sass = require('gulp-sass')
plumber = require('gulp-plumber')
autoprefixer = require('gulp-autoprefixer')
csso = require('gulp-csso')
uglify = require('gulp-terser')
browserSync = require('browser-sync').create()
browserify = require('browserify')
coffeeify = require('coffeeify')
css = require('browserify-css')
zip = require('gulp-zip')
babel = require('gulp-babel')
del = require('del')
slim = require("gulp-slim")
sourcemaps = require('gulp-sourcemaps')
stripCssComments = require('gulp-strip-css-comments')
gulpCopy = require('gulp-copy')


IS_PRODUCTION = false

config =
  styles: ['app/styles/**/*.sass', 'app/styles/**/*.css']
  stylesWatch: ['app/styles/**/*.sass', 'app/styles/**/*.css'],
  html: 'app/index.slim'
  scripts: ['app/scripts/app.js']
  scriptsWatch: ['app/scripts/**/*.coffee', 'app/scripts/**/*.js']
  jsonData: './app/json_data/**/*.json'

  favicon: 'app/favicon.ico'
  images: 'app/images/**/*.*'
  readme: 'README.txt'

  to: './dist'


gulp.task 'html', ->
  gulp.src config.html
    .pipe slim({ pretty: true })
    .pipe gulp.dest(config.to)

gulp.task 'styles:prepare', ->
  gulp.src config.styles
    .pipe plumber()
    .pipe sass()
    .pipe autoprefixer(browsers: ['last 1 version'])
    .pipe stripCssComments()
    .pipe gulpif IS_PRODUCTION, csso()
    .pipe gulp.dest("#{config.to}/styles/")
    # .pipe concat('app.css')

gulp.task 'copy:json', ->
  gulp.src config.jsonData
    .pipe gulpCopy(config.to, { prefix: 1 })

gulp.task 'copy:favicon', ->
  gulp.src config.favicon
    .pipe gulpCopy(config.to, { prefix: 1 })

gulp.task 'copy:images', ->
  gulp.src config.images
    .pipe gulpCopy(config.to, { prefix: 1 })

gulp.task 'copy:readme', ->
  gulp.src config.readme
    .pipe gulpCopy(config.to, {})


babelProps =
  presets: [
    "@babel/preset-env"
  ],
  plugins: [
    "@babel/plugin-transform-object-assign",
    "@babel/plugin-transform-classes",
    "@babel/plugin-transform-template-literals"
    [ "@babel/plugin-proposal-decorators", { "legacy": true } ],
    ["@babel/plugin-proposal-class-properties", { "loose": true }]
  ]


bundle = (bundler) ->
  bundlerStream = bundler
    .bundle()
    .pipe plumber()
    .pipe source('app.js')
    .pipe buffer()
    .pipe concat('app.js')
    .pipe gulpif(IS_PRODUCTION, uglify())
    .pipe gulp.dest(config.to)
    .pipe size(title: "total", gzip: true)
    # .pipe replace('process.env.NODE_ENV', "\"#{process.env.NODE_ENV}\"")

gulp.task "browserify", ->
  bundler = browserify({
    entries: config.scripts
    dest: config.to
    outputName: 'app.js'
    extensions: ['.js', '.coffee']
  })
  .transform("coffeeify", {
    transpile: Object.assign({}, babelProps)
  })
  .transform('babelify', Object.assign({}, babelProps))
  .transform(css, autoInject: true)

  bundler.on('update', () -> bundle(bundler))
  bundle(bundler)


gulp.task "reload", (done) ->
  browserSync.reload()
  done()

gulp.task "serve", () ->
  browserSync.init(server: config.to)
  gulp.watch config.stylesWatch, gulp.series('styles:prepare', 'browserify', 'reload')
  gulp.watch config.scriptsWatch, gulp.series('browserify', 'reload')
  gulp.watch config.html, gulp.series('html', 'reload')
  gulp.watch config.jsonData, gulp.series('copy:json', 'reload')
  gulp.watch config.favicon, gulp.series('copy:favicon', 'reload')

gulp.task 'set_dev_env', (cb) ->
  process.env.NODE_ENV = 'development'
  cb()

gulp.task 'set_prod_env', (cb) ->
  process.env.NODE_ENV = 'production'
  IS_PRODUCTION = true
  cb()

gulp.task 'clean', del.bind(null, config.to)
gulp.task "once", gulp.series('clean', gulp.parallel('html', 'styles:prepare', 'copy:json', 'copy:favicon', 'copy:images', 'copy:readme'), 'browserify')
gulp.task "dev", gulp.series('set_dev_env', 'once', 'serve')
gulp.task "build", gulp.series('once')
gulp.task "buildForDeploy", gulp.series('set_prod_env', 'build')
